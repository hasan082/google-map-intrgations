import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Map Animation and Location Updates',
      home: MapScreen(),
    );
  }
}

class MapScreen extends StatefulWidget {
  const MapScreen({super.key});

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  // LatLng _userLocation;

  @override
  void initState() {
    super.initState();
    // _getUserLocation();
  }

  // Future<void> _getUserLocation() async {
  //   bool serviceEnabled;
  //   LocationPermission permission;
  //
  //   serviceEnabled = await Geolocator.isLocationServiceEnabled();
  //   if (!serviceEnabled) {
  //     // Handle if location services are not enabled
  //     return;
  //   }
  //
  //   permission = await Geolocator.checkPermission();
  //   if (permission == LocationPermission.deniedForever) {
  //     // Handle the case where user has previously denied location permission permanently
  //     return;
  //   }
  //
  //   if (permission == LocationPermission.denied) {
  //     permission = await Geolocator.requestPermission();
  //     if (permission != LocationPermission.whileInUse &&
  //         permission != LocationPermission.always) {
  //       // Handle if permission was denied or not granted
  //       return;
  //     }
  //   }
  //
  //   Position position = await Geolocator.getCurrentPosition(
  //     desiredAccuracy: LocationAccuracy.high,
  //   );
  //
  //   setState(() {
  //     _userLocation = LatLng(position.latitude, position.longitude);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Google Map Screen'),
      ),
      body: GoogleMap(
          initialCameraPosition: const CameraPosition(
              target: LatLng(23.7273096, 90.3812579),
              zoom: 17,
              tilt: 10,
              bearing: 30),
          myLocationEnabled: true,
          myLocationButtonEnabled: true,
          trafficEnabled: true,
          onTap: (LatLng position) {
            log('Marker tapped at ${position.latitude}, ${position.longitude}');
          },
          onLongPress: (LatLng position) {
            log('Marker tapped at ${position.latitude}, ${position.longitude}');
          },
          onMapCreated: (GoogleMapController controller) {
            log('on map created');
          },
          mapType: MapType.normal,
          markers: <Marker>{
            Marker(
                markerId: const MarkerId('Marker-1'),
                position: const LatLng(23.7273096, 90.3812579),
                icon: BitmapDescriptor.defaultMarkerWithHue(
                    BitmapDescriptor.hueAzure),
                infoWindow: InfoWindow(
                    title:
                        "My Location is ${const LatLng(23.7273096, 90.3812579)}")),
          },
          polylines: {
            Polyline(
              polylineId: (const PolylineId('plyline-1')),
              color: Colors.pink,
              width: 4,
              jointType: JointType.round,
              onTap: () {
                log('Tap pn polyline');
              },
              points: const [
                LatLng(23.7273096, 90.3812579),
                LatLng(23.72831008166603, 90.38128472864628),
                LatLng(23.729018174596202, 90.3814771771431),
                LatLng(23.728555627981258, 90.3820551931858),
                LatLng(23.7273096, 90.3812579),
              ],
            ),
          },
          circles: {
            const Circle(
              circleId: (CircleId('circle-1')),
              center: LatLng(23.7273096, 90.3812579),
              radius: 30,
              strokeWidth: 4,
              strokeColor: Colors.red
            ),
            const Circle(
              circleId: (CircleId('circle-2')),
              center: LatLng(23.729018174596202, 90.3814771771431),
              radius: 30,
              strokeWidth: 4,
              strokeColor: Colors.blue
            ),
            const Circle(
                circleId: (CircleId('circle-3')),
                center: LatLng(23.728555627981258, 90.3820551931858),
                radius: 30,
                strokeWidth: 4,
                strokeColor: Colors.green
            ),
          },
        polygons: {
            const Polygon(
              polygonId: PolygonId('polygon-1'),
              strokeColor: Colors.red,
              strokeWidth: 0,
              fillColor: Colors.blue,
              points: [
                LatLng(23.72823825928139, 90.38018602877855),
                LatLng(23.727032312561985, 90.37923652678728),
                LatLng(23.72793040476554, 90.37944842129946),
                LatLng(23.728797490649566, 90.37900552153586),
              ],
            ),
        },
      ),
    );
  }
}
